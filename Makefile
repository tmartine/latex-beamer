all: slides.pdf

slides.pdf: slides.tex sample.bib
	latexmk -lualatex -shell-escape slides.tex
